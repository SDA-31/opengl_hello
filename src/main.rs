#[allow(unused_imports)]
#[macro_use]
extern crate glium;

use glium::{
	glutin::{event::*, event_loop::*, window::*, *},
	index::*,
	*,
};
use std::time::*;

#[derive(Copy, Clone)]
struct Vertex {
	position: [f32; 2],
}
implement_vertex!(Vertex, position);

struct Shape;

trait Shapes {
	fn triangle() -> Vec<Vertex>;
}

impl Shapes for Shape {
	fn triangle() -> Vec<Vertex> {
		vec![
			Vertex {
				position: [-0.5, -0.5],
			},
			Vertex {
				position: [0.0, 0.5],
			},
			Vertex {
				position: [0.5, -0.25],
			},
		]
	}
}

fn main() {
	let event_loop = EventLoop::new();
	let wb = WindowBuilder::new();
	let cb = ContextBuilder::new();
	let _display = Display::new(wb, cb, &event_loop).unwrap();
		let indices = NoIndices(PrimitiveType::TrianglesList);

	let vertex_shader_src = r#"
	#version 140
	in vec2 position;

	uniform float t;

	void main() {
			vec2 pos = position;
			pos.x += t;
			gl_Position = vec4(pos, 0.0, 1.0);
	}
"#;

	let fragment_shader_src = r#"
	#version 140
	out vec4 color;

	void main() {
			color = vec4(0.0, 1.0, 0.0, 1.0);
	}
"#;

	let program =
		Program::from_source(&_display, vertex_shader_src, fragment_shader_src, None).unwrap();

	let mut t: f32 = -0.5;
	event_loop.run(move |event, _, control_flow| {

		match event {
			Event::WindowEvent { event, .. } => match event {
				WindowEvent::CloseRequested => {
					*control_flow = ControlFlow::Exit;
					return;
				}
				_ => return,
			},
			Event::NewEvents(cause) => match cause {
				StartCause::ResumeTimeReached { .. } => (),
				StartCause::Init => (),
				_ => return,
			},
			_ => return,
		}

		let next_frame_time = Instant::now() + Duration::from_nanos(16_666_667);
		*control_flow = ControlFlow::WaitUntil(next_frame_time);

		let vertex_buffer = VertexBuffer::new(&_display, &Shape::triangle()).unwrap();

		t += 0.002;
		if t > 0.5 {
			t = -0.5;
		}

		let mut target = _display.draw();
		target.clear_color(0.0, 0.0, 1.0, 1.0);
		target
			.draw(
				&vertex_buffer,
				&indices,
				&program,
				&uniform! { t: t },
				&Default::default(),
			)
			.unwrap();
		target.finish().unwrap();
	});
}
